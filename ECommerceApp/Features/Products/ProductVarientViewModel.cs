using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerceApp.Features.Products
{
    public class ProductVarientViewModel
    {
        public int ProductId { get; set; }
        public string Name{get;set;}
        public String Thumbnail { get; set; }
        public int ColourId { get; set; }
        public string Colour{get;set;}
        public int StorageId { get; set; }
        public string Capacity{get;set;}
        public decimal Price { get; set; }
    }
}