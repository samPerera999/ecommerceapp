using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ECommerceApp.Features.Products
{
    public class ProductDetailsViewModel
    {
        public int Id { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public String Description { get; set; }
        public decimal Price { get; set; }
        public String Thumbnail { get; set; }
        public IEnumerable<string> Images { get; set; }
        public IEnumerable<string> Features { get; set; }  
        public IEnumerable<ProductVarientViewModel> Variants { get; set; }
    }
}
