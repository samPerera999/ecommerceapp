using System.Reflection;
using System.Drawing;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System;
using System.Threading.Tasks;
using ECommerceApp.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ECommerceApp.Data;

namespace ECommerceApp.Features.Filters
{
    [Route("api/[controller]")]
    public class FiltersController: Controller
    {
        private readonly ECommerceAppContext _db;

        public FiltersController(ECommerceAppContext db)
        {
        _db = db;
        }

        public async Task<IActionResult> Get()
        {
            var brands = await _db.Brands
                .Select(p => p.Name)
                .ToListAsync();

            var storage = await _db.Storage
                .Select(p => p.Capacity.ToString()+" GB")
                .ToListAsync();

            var colours = await _db.Colours
                .Select(p => p.Name)
                .ToListAsync();

            var os = await _db.OS
                .Select(p => p.Name)
                .ToListAsync();

            var features = await _db.Features
                .Select(p => p.Name)
                .ToListAsync();

            return Ok(new FiltersListViewModel
            {
                Brands = brands,
                Storage = storage,
                Colours = colours,
                OS = os,
                Features = features
            });
        }
    }
}