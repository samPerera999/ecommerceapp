import Vue from 'vue';
import Router from 'vue-router';

//import page components
import Account from "../pages/Account.vue"
import Catalogue from "../pages/Catalogue.vue";
import Product from "../pages/Product.vue";
import Cart from "../pages/Cart.vue";
import Checkout from "../pages/Checkout.vue";

//admin panel 
import AdminIndex from "../pages/admin/Index.vue";
import AdminOrders from "../pages/admin/Orders.vue";
import AdminProducts from "../pages/admin/Products.vue";
import AdminCreateProduct from "../pages/admin/CreateProduct.vue";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: "/account", 
            component: Account, 
            name: "account", 
            meta: {
                requiresAuth: true,
                role: "Customer"
            }
        },
        {
            path: "/cart",
            component: Cart,
            name: "cart",
            meta: {
                role: "Customer"
            }
        },
        {
            path: "/checkout", 
            component: Checkout, 
            name: "checkout", 
            meta: {
                requiresAuth: true,
                role: "Customer"
            }
        },
        {
            path: "/products",
            component: Catalogue,
            name: "products"
        },
        {
            path: "/products/:slug",
            component: Product
        },
        {
            path: "/admin", 
            component: AdminIndex,             
            meta: {
                requiresAuth: true,
                role: "Admin"
            },
            redirect: "/admin/orders",
            children: [
                {
                    path: "orders",
                    component: AdminOrders
                },
                {
                    path: "products",
                    component: AdminProducts
                },
                {
                    path: "products/create",
                    component: AdminCreateProduct
                }
            ]
        },
        {
            path: "*",
            redirect: "/products"
        }
    ],
});
