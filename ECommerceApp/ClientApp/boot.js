import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import NProgress from 'nprogress';
import VueToastr from '@deveodk/vue-toastr';
import VueNotification from "@kugatsu/vuenotification";
import axios  from "axios";
import VeeValidate from "vee-validate";

//helpers
import "./helpers/validation";

import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css';

import App from './components/App.vue';
import router from './router';
import store from './store';

Vue.use(BootstrapVue);
Vue.use(VueToastr, {
  defaultPosition: 'toast-top-right',
  defaultType: 'info',
  defaultTimeout: 5000
});
Vue.use(VueNotification, {
  timer: 5,
  position: 'topRight',
  showCloseIcn: true,  
});
Vue.use(VeeValidate);//vee validation


// filters
import {currency, date} from './filters';

Vue.filter("currency", currency);
Vue.filter("date", date);

/*Add authentication headers on application statrt up
other wise if user refreshed the browser authntication headers including bearer token will be cleared
so user cannot access to authorized api endpoints eventhough user is successfully authenticated*/
const initialStore = localStorage.getItem("store");

if (initialStore) {
  store.commit("initialise", JSON.parse(initialStore));    
  if (store.getters["user/isAuthenticated"]) {    
    axios.defaults.headers.common["Authorization"] = `Bearer ${store.state.user.auth.access_token}`;      
  }
}

router.beforeEach((to, from, next) => {
  NProgress.start();
  if (to.matched.some(route => route.meta.requiresAuth)) {
    if (!store.getters["user/isAuthenticated"]) {
      store.commit("user/showAuthModal");
      next({ path: from.path, query: { redirect: to.path } });
    } 
    else {     
      if (to.matched.some(route => route.meta.role
            && store.getters["user/isInRole"](route.meta.role))) {
        next();
      }
      else if (!to.matched.some(route => route.meta.role)) {
        next();
      }
      else {
        next({ path: "/" });
      }
    }
  } 
  else {
    if (to.matched.some(route => route.meta.role
          && (!store.getters["user/isAuthenticated"] || store.getters["user/isInRole"](route.meta.role)))) {
      next();
    }
    else {
      if (to.matched.some(route => route.meta.role)) {
        next({ path: "/" });
      }
      next();
    }
  }
});

router.afterEach((to, from) => {
  NProgress.done();
});

new Vue({
  render: (h) => h(App),
  router,
  store  
}).$mount('#app-root');
