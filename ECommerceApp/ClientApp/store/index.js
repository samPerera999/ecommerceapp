import Vue from "vue";
import Vuex from 'vuex';

import cartModule from './modules/cart';
import userModule from './modules/user';

Vue.use(Vuex);

const store  = new Vuex.Store({
    namespaced: true,
    mutations:{
        initialise(state, payload){
            Object.assign(state, payload);
        }
    },
    modules: {
        cart : cartModule,
        user : userModule,
    }
});

store.subscribe((mutation, state) => {
    localStorage.setItem("store", JSON.stringify(state));
});

export default store;