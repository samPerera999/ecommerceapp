import axios from 'axios';

export default{
    namespaced: true,
    state: {
        auth: null,
        showAuthModal: false,
        loading: false,        
    },
    mutations: {
        hideAuthModal(state){
            state.showAuthModal = false;
        },
        logOut(state){
            state.auth = null;
        },
        loginRequest(state){
            state.loading = true;
        },
        loginError(state){
            state.loading = false;
        },
        loginSuccess(state, payload){
            state.auth = payload;
            state.loading = false;
        },
        registerError(state){            
            state.loading = false;
        },
        registerRequest(state){
            state.loading = true;
        },
        registerSuccess(state){            
            state.loading = false;
        },
        showAuthModal(state){
            state.showAuthModal = true;
        },           
    },
    actions: {
        /*this action also return a promise so when we dispatch this request from componennt
        we can wait before perfomain anny additional componnent level operations */
        login({commit}, payload){
            return new Promise((resolve, reject) => {
                commit("loginRequest");
                axios
                  .post("/api/token", payload)
                  .then(response => {
                    const auth = response.data;
                    axios.defaults.headers.common["Authorization"] = `Bearer ${
                      auth.access_token
                    }`;
                    commit("loginSuccess", auth);
                    commit("hideAuthModal");
                    resolve(response);
                  })
                  .catch(error => {
                    commit("loginError");
                    delete axios.defaults.headers.common["Authorization"];
                    reject(error.response);
                  });
              });
        },
        logout({ commit }) {
            commit("logOut");
            delete axios.defaults.headers.common["Authorization"];
        },
        register({ commit }, payload) {
            return new Promise((resolve, reject) => {
              commit("registerRequest");
              axios
                .post("/api/account", payload)
                .then(response => {
                  commit("registerSuccess");
                  resolve(response);
                })
                .catch(error => {
                  commit("registerError");
                  reject(error.response);
                });
            });
        },
    },
    getters: {
        isAuthenticated(state){
            return (
                state.auth != null &&
                state.auth.accessToken !== null &&
                new Date(state.auth.access_token_expiration) > new Date()
            );
        },

        isInRole: (state, getters) => (role) => {           
            const result = getters.isAuthenticated && state.auth.roles.indexOf(role) > -1;            
            return result;
        },
        
    }
}