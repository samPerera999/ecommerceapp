import axios from 'axios';

export default{
    namespaced: true,
    state: {
        cart: [],
    },
    mutations: {
        addProductToCart(state, product){
            product.quantity = 1;
            state.cart.push(product);
        },
        clearCartItems(state){
            state.cart = [];
        },
        removeProductFromCart(state, index){
            state.cart.splice(index,1);
        },
        setProductQuantity(state, payload){
            let cartItem = Object.assign({}, state.cart[payload.index]);
            cartItem.quantity = payload.quantity;

            state.cart.splice(payload.index,1,cartItem);
        },
        updateProductQuantity(state, index){
            let cartItem = Object.assign({}, state.cart[index]);
            cartItem.quantity++;

            state.cart.splice(index,1,cartItem);
        },             
    },
    actions: {
        addProductToCart({state, commit}, product){
            const index = state.cart.findIndex(
                i => 
                    i.productId === product.productId &&
                    i.colourId === product.colourId &&
                    i.storageId === product.storageId
            );
            if(index >= 0){
                commit("updateProductQuantity", index);
            }else{
                commit("addProductToCart", product);
            }
        },
        removeProductFromCart({state, commit}, product){
            const index = state.cart.findIndex(
                i => 
                    i.productId === product.productId &&
                    i.colourId === product.colourId &&
                    i.storageId === product.storageId
            );
            commit("removeProductFromCart", index);
        },
        setProductQuantity({ state, commit }, payload){
            const index = state.cart.findIndex(
                i =>
                    i.productId === payload.product.productId &&
                    i.colourId === payload.product.colourId &&
                    i.storageId === payload.product.storageId
            );
            if (payload.quantity > 0) {
              payload.index = index;
              commit("setProductQuantity", payload);
            } else {
              commit("removeProductFromCart", index);
            }
        },
    },
    getters: {
        shoppingCartItemCount(state){
            const reducer = (totalQuantity, cartItem) => totalQuantity + cartItem.quantity;

            return state.cart.reduce(reducer,0);
        },
        shoppingCartTotal(state){
            const reducer = (sum, cartItem) => sum + cartItem.price * cartItem.quantity;

            return state.cart.reduce(reducer,0);//0 - starting point of the array
        },
    }
}