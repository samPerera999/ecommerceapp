using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ECommerceApp.Core.Entities
{
  public class OS
  {
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }

    public List<Product> Products = new List<Product>();
  }
}