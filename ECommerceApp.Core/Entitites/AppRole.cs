using Microsoft.AspNetCore.Identity;

namespace ECommerceApp.Core.Entities
{
    public class AppRole : IdentityRole<int>
    {
        public AppRole() { }

        public AppRole(string name)
        {
            Name = name;
        }
    }
}